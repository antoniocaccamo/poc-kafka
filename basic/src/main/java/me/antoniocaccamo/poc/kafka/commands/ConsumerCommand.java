package me.antoniocaccamo.poc.kafka.commands;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import picocli.CommandLine;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

/**
 * @author antoniocaccamo on 05/09/2019
 */
@CommandLine.Command(
        name = "consumer",
        description = "consumer demo java client",
        mixinStandardHelpOptions = true
) @Slf4j
public class ConsumerCommand implements Runnable {

    @CommandLine.Option(required = true,
            description = "bootstrap-server as host:port ",
            names = {
                    "--bootstrap-server"
            },
            defaultValue = "localhost:9092"

    )
    private String bootstrapServer;

    @CommandLine.Option(
            required = true,
            names = {"--topics"},
            description = "list of topics to listen to",
            paramLabel = "topic",
            split = ","
    )
    private String[] topics;

    @CommandLine.Option(
            required = true,
            names = {"--group-id"}
    )
    private String groupId;

    @CommandLine.Option(
            names = {"--with-thread"},
            defaultValue = "false"
    )
    private Boolean withThread;


    @Override
    public void run() {

        // step 1 :
        //  consunmer properties
        Properties props = new Properties();
        props.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG       , bootstrapServer);
        props.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG  , StringDeserializer.class.getName());
        props.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.setProperty(ConsumerConfig.GROUP_ID_CONFIG                , groupId);
        props.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG       , "earliest");

        log.info("producer props {} ", props);

        if ( ! withThread ) {
            // step 2 :
            log.info("creating consumer ..");
            KafkaConsumer<String, String> kafkaConsumer = new KafkaConsumer<String, String>(props);

            // step 3 :
            log.info("subscribe consumer to topics {}  ..", topics);
            kafkaConsumer.subscribe(Arrays.asList(topics));

            // poll new data :
            log.info("polling new data ..");
            while (true) {
                ConsumerRecords<String, String> records = kafkaConsumer.poll(Duration.ofMillis(100));
                records.forEach(consumerRecord -> {
                    log.info(
                            "topic : {} partition : {} offset : {} key {} value : {}",
                            consumerRecord.topic(),
                            consumerRecord.partition(),
                            consumerRecord.offset(),
                            consumerRecord.key(),
                            consumerRecord.value()
                    );
                });
            }
        } else {

            CountDownLatch latch = new CountDownLatch(1);
            ConsumerRunnable runnable = new ConsumerRunnable(latch, props, bootstrapServer, topics);
            new Thread(runnable).start();

            Runtime.getRuntime().addShutdownHook( new Thread( () -> {
                log.info("caught");
                runnable.shutdown();
                try {
                    latch.await();
                } catch (InterruptedException e) {

                    e.printStackTrace();
                }
                log.info("application exited");
            } ));

            try {
                latch.await();
            } catch (InterruptedException e) {

                e.printStackTrace();
            }
            log.info("application exited");
        }

    }

    protected class ConsumerRunnable implements Runnable {

        private final CountDownLatch latch;
        private final Properties consumerProperties;
        private final String bootstrapServer;
        private final String[] topics;
        private KafkaConsumer<String, String> kafkaConsumer;

        public ConsumerRunnable(CountDownLatch latch, Properties consumerProperties, String bootstrapServer, String[] topics) {
            this.latch = latch;
            this.consumerProperties = consumerProperties;
            this.bootstrapServer = bootstrapServer;
            this.topics = topics;
        }

        @Override
        public void run() {

            // step 2 :
            log.info("creating consumer ..");
            kafkaConsumer = new KafkaConsumer<String, String>(consumerProperties);

            // step 3 :
            log.info("subscribe consumer to topics {}  ..", topics);
            kafkaConsumer.subscribe(Arrays.asList(topics));

            // poll new data :
            log.info("polling new data ..");
            try {
                while (true) {
                    ConsumerRecords<String, String> records = kafkaConsumer.poll(Duration.ofMillis(100));
                    records.forEach(consumerRecord -> {
                        log.info(
                                "topic : {} partition : {} offset : {} key {} value : {}",
                                consumerRecord.topic(),
                                consumerRecord.partition(),
                                consumerRecord.offset(),
                                consumerRecord.key(),
                                consumerRecord.value()
                        );
                    });
                }
            } catch (WakeupException e) {
                log.info("received shutdown signal");
                e.printStackTrace();
            } finally {
                kafkaConsumer.close();
                latch.countDown();
            }
        }

        public void shutdown() {
            kafkaConsumer.wakeup();
        }
    }
}
