package me.antoniocaccamo.poc.kafka;

import lombok.extern.slf4j.Slf4j;
import me.antoniocaccamo.poc.kafka.commands.ConsumerCommand;
import me.antoniocaccamo.poc.kafka.commands.ProducerCommand;
import picocli.CommandLine;

/**
 * @author antoniocaccamo on 05/09/2019
 */

@CommandLine.Command(
        name = "kafka-demo",
        description = "kakfa demo ",
        subcommands = {
                ConsumerCommand.class,
                ProducerCommand.class,
        },
        mixinStandardHelpOptions = true
) @Slf4j
public class Main implements Runnable {

    public static void main(String[] args) {
        new CommandLine(new Main()).execute(args);
    }

    @Override
    public void run() {

        CommandLine.usage(this, System.out);

    }
}
