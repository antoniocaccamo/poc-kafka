package me.antoniocaccamo.poc.kafka.commands;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import java.util.Properties;

/**
 * @author antoniocaccamo on 05/09/2019
 */
@Command(
        name = "producer",
        description = "producer demo java client",
        mixinStandardHelpOptions = true
) @Slf4j
public class ProducerCommand implements Runnable {

    @Option(required = true,
            description = "as host:port ",
            names = {
                "--bootstrap-server"
            },
            defaultValue = "localhost:9092"

    )
    private String bootstrapServer;

    @Option(required = true,
            names = {"--topic"}
    )
    private String topic;

    @Option(
            names = {"--key"}
    )
    private String key;


    @Option(required = true,
            names = {"--value"}
    )
    private String value;

    @Option(names = {"--safe"},
            description = "safe producer"
    )
    private Boolean safe;

    @Option(names = {"--high-throughput"},
            description = "high-throughput"
    )
    private Boolean highThroughput;

//    public static void main(String[] args) {
//        new CommandLine(new ProducerDemo()).execute(args);
//    }

    public void run() {

        KafkaProducer<String, String> kafkaProducer = null;

        // step 1 :
        //  producer properties
        Properties props = new Properties();
        props.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG     , bootstrapServer);
        props.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG  , StringSerializer.class.getName());
        props.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        
        if (safe) {
            props.setProperty(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, String.valueOf(true));
            // already setted by enable.idempotence =true
            // props.setProperty(ProducerConfig.ACKS_CONFIG                          , "all");
            // props.setProperty(ProducerConfig.RETRIES_CONFIG, String.valueOf(Integer.MAX_VALUE));
            // props.setProperty(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, String.valueOf(5));
        }

        if (highThroughput) {
            props.setProperty(ProducerConfig.COMPRESSION_TYPE_CONFIG, "snappy");       // nope at broker/consumer
            props.setProperty(ProducerConfig.LINGER_MS_CONFIG       , "20");           // 20ms
            props.setProperty(ProducerConfig.RETRIES_CONFIG, String.valueOf(32*1024)); // 32 KB
        }

        log.info("producer props {} ", props);

        // step 2 :
        log.info("creating producer ..");
        kafkaProducer = new KafkaProducer<String, String>(props);

        // step 3 :
        log.info("creating producer record ..");
        ProducerRecord<String, String> record = null;

        if (StringUtils.isEmpty(key)) {
            record = new ProducerRecord<>(topic, value);
        } else {
            record = new ProducerRecord<>(topic, key, value);
        }

        // step 4 :
        log.info("sending record : topic {} key {} value {} ..",
                topic ,
                StringUtils.isEmpty(key) ? StringUtils.EMPTY : key,
                value
        );
        kafkaProducer.send(record , (recordMetadata, e) -> {
            if ( e != null ){
                log.error("error occurred ", e);
            } else {
                log.info("recordMetadata {}", recordMetadata);
            }
        });

        // step 5 :
        log.info("flushing ..");
        kafkaProducer.flush();

        // step 6 :
        log.info("closing ..");
        kafkaProducer.close();

    }

}