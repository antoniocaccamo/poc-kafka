# kafka theory

![images/concepts.png](images/concepts.png)
 
## topics, partitions and offset
- topic : a particolar stream of data, identified by name, splitted in partitions
- partition: is ordered, each message in a partition has an incremental id, called offset
- order is guaranteed only within a partition
- data is kept for a limited time ( default a week)
- when data is written to a partition is immutable, it can't be changed
- data is ranmdoly assigned to a partition, unless a key is assigned  

## brokers
- kafka cluster is composed my multiple brokers (servers)
- each broker is identified with an Integer ID
- each broker contains certain topic partitions
- when connected to any broker, you'll be connected to the entire cluster
  
## topic replications
- usually 2 or 3
- if a broker is down, others can server data
- only one broker can be a leader for a given partition
- each partition has one leader and multiple in-sync replica

## producers
- write data to topic
- automatically know to which broker and partition to write to
- in case of broker failure, producers will automatically recover
  
### acks

- can choose to receive ack for data writes
	 1. acks=0 no ack
	 2. acks=1 ack from leader
	 3. acks=2 acks from leader and replicas

### message key
- producers can choose to send a key with the message
- if key=null, data is sent in round robin to a broker
- if key is set, all message with same key will always to same partition

## consumers
- read data from a topic
- know which broker to read from
- know how to recover
- data is read in order within each partition  

### consumer groups
- consumers read data in consumer groups
- each consumer in a group reads from exclusive partitions
- it there're mor consumers than partitions, someone will be inactive
- usually you don't have inactive consumers, you just have as many consumers as partitions at most. So that's why it's so important when you choose a number of partitions for your topic. If you want to have a high number of consumers, you need to have a high number of partitions
- 
### consumer offsets
- kafka, basically, has a capability that is able to restore the offsets at which a consumer group has been reading
- when a consumer, basically, in a group, we've seen the group before, when this has processed data and done what it's need to do, then you will be committing the offsets into Kafka
- if a consumer dies, it will be able to read back exactly from where it left off

#### semantics of commit offsets
- consumer can choose when to commit offset , 3 delivery semantics:
	1. at most once 
	    - offsets committed as soon as messages are received
	    - if processsing goes wrong, message will be lost ( can't be read again)
	2. at least once (the preferred)
		- offsets committed after messages are processed
		- if processsing goes wrong, message will be be read again
		- message **duplication** can be result, so processing must be idempotent
	3. exactly one
	    - can be reached for kafka => kafka workflow using kafka streaming api
	    - for kafka => external system workflow use a **idempotent** consumer
	    - 
## broker discovery
- every kafka broker basically it's called a bootstrap server
- connecting to one broker, you'll be connected all the other servers in the entire cluster
 - each broker knows all about others ones, topics and partitions (metadata)

## zookeeper
- kakfa can't work without Zookeeper
- manages brokers
- helps in performing leader election for partitions
- sends notifications to kafka in case of changes ( new/delete topic, brokers dies/come up, etc..)
- by design operates with an odd number of server ( 3, 5, ..)
- has leader (handles writes), and others are followers (handle reads)
- offset message is kept by kafka   

## kafka guarantees
- ***messages are appended to a topic-partition in the order they are sent***

- ***consumers  read messages in the order they're stored in  topic-partition***

- ***with a replicaton factor of N, producers and consumers can tolerate up to N-1 brokers being down***

- ***so replication factor 3 is a good choice***

- ***as long as the number of partitions remains constant for a topic, message with same key will go to same partition***



