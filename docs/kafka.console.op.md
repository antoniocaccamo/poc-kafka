# poc-kafka


Please refer to [theory](./docs/kafka.theory.md)

## basics operations
start first zookeeper

    zookeeper-server-start.sh config/zookeeper.properties

and then start kafka broker

    kafka-server-start.sh config/server.properties

### topics

- topic list

		kafka-topics.sh --zookeeper localhost:2181 --list


- topic creation

        kafka-topics.sh --zookeeper localhost:2181 \
            --create --topic topic_first           \
            --partitions 3                         \
            --replication-factor 1

- topic describe

    
        kafka-topics.sh --zookeeper localhost:2181 --describe --topic topic_first

    and results is

        Topic:topic_first	PartitionCount:3	ReplicationFactor:1	Configs:
        Topic: topic_first	Partition: 0	Leader: 0	Replicas: 0	Isr: 0
        Topic: topic_first	Partition: 1	Leader: 0	Replicas: 0	Isr: 0
        Topic: topic_first	Partition: 2	Leader: 0	Replicas: 0	Isr: 0

- topic deletion

        kafka-topics.sh --zookeeper localhost:2181 --delete --topic topic_first

### console producer

    kafka-console-producer.sh --broker-list localhost:9092 \
        --topic topic_first

can add extra property

    kafka-console-producer.sh --broker-list localhost:9092 \
        --topic topic_first                                \
        --producer-property acks=all


if topic doesn't exist, it'll be created with default value of `num.partitions` in file `config/server.config`         

### console consumer

listening for further messages 

    kafka-console-consumer.sh --bootstrap-server localhost:9092 \
		--topic topic_first

listening for messages from the beginning

    kafka-console-consumer.sh --bootstrap-server localhost:9092 \
		--topic topic_first --from-beginning
   

## consumer group

group should be the application name

having this case:

	kafka-console-consumer.sh --bootstrap-server localhost:9092 \
        --topic topic_first --group my-app

and

    kafka-console-consumer.sh --bootstrap-server localhost:9092 \
        --topic topic_first --group my-app

messages are received indifferently by one of these

in case of specifying `--from-beginning`, as:

    kafka-console-consumer.sh --bootstrap-server localhost:9092 \
        --topic topic_first --group my-app --from-beginning

when consumer restart, no messages will be deplayed because message offset was committed for group `my-app`


### group operation

- list consumer groups

        kafka-consumer-groups.sh --bootstrap-server localhost:9092 --list

-   describe consumer group

        kafka-consumer-groups.sh --bootstrap-server localhost:9092 --describe --group my-app-2

    and result is

    ```
    GROUP           TOPIC           PARTITION  CURRENT-OFFSET  LOG-END-OFFSET  LAG CONSUMER-ID                                     HOST        CLIENT-ID
    my-app-2        topic_first     0          9               9               0   consumer-1-cd7e4544-5a68-438e-8a4c-207131fe75b8 /127.0.0.1  consumer-1
    my-app-2        topic_first     1          10              10              0   consumer-1-cd7e4544-5a68-438e-8a4c-207131fe75b8 /127.0.0.1  consumer-1
    my-app-2        topic_first     2          9               9               0   consumer-1-f1c6ff7c-5f8a-49e1-a1d6-234cedf2fd42 /127.0.0.1  consumer-1
    ```

- reset offset

    1. at the beginning

            kafka-consumer-groups.sh --bootstrap-server localhost:9092 \
                --reset-offsets --to-earliest                          \
                --group my-app-2                                       \
                --topic topic_first                                    \
                --execute

        and result is

        ```
        GROUP                          TOPIC                          PARTITION  NEW-OFFSET     
        my-app-2                       topic_first                    0          0              
        my-app-2                       topic_first                    2          0              
        my-app-2                       topic_first                    1          0
        ```
    1. shift back by N 

            kafka-consumer-groups.sh --bootstrap-server localhost:9092 \
                --reset-offsets --shift-by -2                          \
                --group my-app-2                                       \
                --topic topic_first                                    \
                --execute
        
        and result is

        ```
        GROUP                          TOPIC                          PARTITION  NEW-OFFSET     
        my-app-2                       topic_first                    0          7              
        my-app-2                       topic_first                    2          7              
        my-app-2                       topic_first                    1          8
        ```
    


