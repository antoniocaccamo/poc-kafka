# poc-kafka

Sample Java console app to interact whith  Apache Kafka

### console basics operations

For console opearation, please refer to this [document](./docs/kafka.console.op.md)

## Ho to use

The class `me.antoniocaccamo.poc.kafka.Main` is a Picocli Command application

     mvn clean package
     
and after

    java -jar target/poc-kafka.jar -h
    
to see all available operations

### producer

```bash
$ java -jar target/poc-kafka.jar producer -h
    Usage: kafka-demo producer [-hV] [--bootstrap-server=<bootstrapServer>]
                               [--key=<key>] --topic=<topic> --value=<value>
    producer demo java client
          --bootstrap-server=<bootstrapServer>
                            as host:port
      -h, --help            Show this help message and exit.
          --key=<key>
          --topic=<topic>
      -V, --version         Print version information and exit.
          --value=<value>
```

example :

```
$ java -jar target/poc-kafka.jar producer --topic xxx --value yyy --key zzz
```

### consumer

```bash
$ java -jar target/poc-kafka.jar consumer
    Usage: kafka-demo consumer [-hV] [--bootstrap-server=<bootstrapServer>]
                               --group-id=<groupId> --topics=topic[,topic...]
                               [--topics=topic[,topic...]]...
    consumer demo java client
          --bootstrap-server=<bootstrapServer>
                                 bootstrap-server as host:port
          --group-id=<groupId>
      -h, --help                 Show this help message and exit.
          --topics=topic[,topic...]
                                 list of topics to listen to
      -V, --version              Print version information and exit.

```
example :
```bash
$ java -jar target/poc-kafka.jar consumer --group-id java-app --topics topic_first
```
        


